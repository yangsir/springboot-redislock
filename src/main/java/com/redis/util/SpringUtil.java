package com.redis.util;

import com.redis.exception.RedisLockException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

/**
 * @author
 * @version V1.0
 * @Description
 * @date 2018-01-29 16:20
 **/
public class SpringUtil {

    public static ApplicationContext applicationContext;

    public static <T> T getBean(Class<T> className) throws BeansException, RedisLockException {
        if (applicationContext == null) {
            throw new RedisLockException("请开始EnableRedisLockConfiguration注解");
        }
        return applicationContext.getBean(className);
    }
}
